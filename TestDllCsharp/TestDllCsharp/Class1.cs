﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TestDllCsharp
{
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class Class1
    {
        [ComVisible(true)]
        public string getStringTest()
        {
            return "test csharp dll";
        }

        
        public Class1() { }
    }
}
