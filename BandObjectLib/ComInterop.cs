//	This file is a part of the Command Prompt Explorer Bar project.
// 
//	Copyright Pavel Zolnikov, 2002
//
//			declarations of some COM interfaces and structues

using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace BandObjectLib
{

	abstract class ExplorerGUIDs
	{
		public static readonly Guid IID_IWebBrowserApp = new Guid("{0002DF05-0000-0000-C000-000000000046}");
		public static readonly Guid IID_IUnknown =		 new Guid("{00000000-0000-0000-C000-000000000046}");
	}



	[Flags]
	public enum DBIM : uint
	{
          DBIMF_NORMAL = 0x0000,

            /// <summary>
            /// Windows XP and later: The band object is of a fixed sized and position. With this flag, a sizing grip is not displayed on the band object.
            /// </summary>
            DBIMF_FIXED = 0x0001,

            /// <summary>
            /// DBIMF_FIXEDBMP
            /// Windows XP and later: The band object uses a fixed bitmap (.bmp) file as its background. Note that backgrounds are not supported in all cases, so the bitmap may not be seen even when this flag is set.
            /// </summary>
            DBIMF_FIXEDBMP = 0x0004,

            /// <summary>
            /// The height of the band object can be changed. The ptIntegral member defines the step value by which the band object can be resized.
            /// </summary>
            DBIMF_VARIABLEHEIGHT = 0x0008,

            /// <summary>
            /// Windows XP and later: The band object cannot be removed from the band container.
            /// </summary>
            DBIMF_UNDELETEABLE = 0x0010,

            /// <summary>
            /// The band object is displayed with a sunken appearance.
            /// </summary>
            DBIMF_DEBOSSED = 0x0020,

            /// <summary>
            /// The band is displayed with the background color specified in crBkgnd.
            /// </summary>
            DBIMF_BKCOLOR = 0x0040,

            /// <summary>
            /// Windows XP and later: If the full band object cannot be displayed (that is, the band object is smaller than ptActual, a chevron is shown to indicate that there are more options available. These options are displayed when the chevron is clicked.
            /// </summary>
            DBIMF_USECHEVRON = 0x0080,

            /// <summary>
            /// Windows XP and later: The band object is displayed in a new row in the band container.
            /// </summary>
            DBIMF_BREAK = 0x0100,

            /// <summary>
            /// Windows XP and later: The band object is the first object in the band container.
            /// </summary>
            DBIMF_ADDTOFRONT = 0x0200,

            /// <summary>
            /// Windows XP and later: The band object is displayed in the top row of the band container.
            /// </summary>
            DBIMF_TOPALIGN = 0x0400,

            /// <summary>
            /// Windows Vista and later: No sizing grip is ever displayed to allow the user to move or resize the band object.
            /// </summary>
            DBIMF_NOGRIPPER = 0x0800,

            /// <summary>
            /// Windows Vista and later: A sizing grip that allows the user to move or resize the band object is always shown, even if that band object is the only one in the container.
            /// </summary>
            DBIMF_ALWAYSGRIPPER = 0x1000,

            /// <summary>
            /// Windows Vista and later: The band object should not display margins.
            /// </summary>
            DBIMF_NOMARGINS = 0x2000

            // The author had it totally wrong...
            // he had used dbi.dwModeFlags = DBIM.TITLE | DBIM.ACTUAL | DBIM.MAXSIZE | DBIM.MINSIZE | DBIM.INTEGRAL;
            // which means 10, 8, 2, 1, 4
            // which means undeleatable, variable height, ?, fixed, DBIMF_FIXEDBMP

		//MINSIZE   =0x0001,
		//MAXSIZE   =0x0002,
		//INTEGRAL  =0x0004,
		//ACTUAL    =0x0008,
		//TITLE     =0x0010,
		//MODEFLAGS =0x0020,
		//BKCOLOR   =0x0040
	}


	[StructLayout(LayoutKind.Sequential,CharSet=CharSet.Unicode)]
	public struct DESKBANDINFO
	{
		public UInt32		dwMask;
		public Point	    ptMinSize;
		public Point		ptMaxSize;
		public Point		ptIntegral;
		public Point		ptActual;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst=255)]
		public String		wszTitle;
		public DBIM			dwModeFlags;
		public Int32		crBkgnd;
	};

	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("FC4801A3-2BA9-11CF-A229-00AA003D7352")] 
	public interface IObjectWithSite
	{
		void SetSite([In ,MarshalAs(UnmanagedType.IUnknown)] Object pUnkSite);
		void GetSite(ref Guid riid, [MarshalAs(UnmanagedType.IUnknown)] out Object ppvSite);
	}

	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00000114-0000-0000-C000-000000000046")] 
	public interface IOleWindow
	{
		void GetWindow(out System.IntPtr phwnd);   
		void ContextSensitiveHelp([In] bool fEnterMode);
	}

	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("012dd920-7b26-11d0-8ca9-00a0c92dbfe8")] 
	public interface IDockingWindow
	{
		void GetWindow(out System.IntPtr phwnd);   
		void ContextSensitiveHelp([In] bool fEnterMode);

		void ShowDW([In] bool fShow);
		void CloseDW([In] UInt32  dwReserved);
		int ResizeBorderDW(
			IntPtr prcBorder,
			[In, MarshalAs(UnmanagedType.IUnknown)] Object punkToolbarSite,
			bool fReserved);
	}


	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("EB0FE172-1A3A-11D0-89B3-00A0C90A90AC")] 
	public interface IDeskBand
	{
		void GetWindow(out System.IntPtr phwnd);   
		void ContextSensitiveHelp([In] bool fEnterMode);

		void ShowDW([In] bool fShow);
		void CloseDW([In] UInt32  dwReserved);
		
		int ResizeBorderDW(
			IntPtr prcBorder,
			[In, MarshalAs(UnmanagedType.IUnknown)] Object punkToolbarSite,
			bool fReserved);

		void GetBandInfo(
			UInt32 dwBandID,
			UInt32 dwViewMode,
			ref DESKBANDINFO pdbi);
	}


	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000010c-0000-0000-C000-000000000046")]
	public interface IPersist
	{
		void GetClassID(out Guid pClassID);
	}

	
	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00000109-0000-0000-C000-000000000046")]
	public interface IPersistStream
	{
		void GetClassID(out Guid pClassID);

		int IsDirty ();

		void Load (/*[In, MarshalAs(UnmanagedType.Interface)]*/ Object pStm);

		void Save ([In, MarshalAs(UnmanagedType.Interface)] Object pStm,
			[In] bool fClearDirty);

		int GetSizeMax ([Out] out UInt64 pcbSize);
	}


	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
	public interface _IServiceProvider
	{
		void QueryService(
			ref Guid guid, 
			ref Guid riid, 
			[MarshalAs(UnmanagedType.Interface)] out Object Obj);
	}


	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("68284faa-6a48-11d0-8c78-00c04fd918b4")]
	public interface IInputObject
	{
		void UIActivateIO(Int32 fActivate, ref MSG  msg);
		
		[PreserveSig]
		//[return:MarshalAs(UnmanagedType.Error)]
		Int32 HasFocusIO();

		[PreserveSig]
		Int32 TranslateAcceleratorIO(ref MSG msg);
	}

	[ComImport]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("f1db8392-7331-11d0-8c99-00a0c92dbfe8")]
	public interface IInputObjectSite
	{
		[PreserveSig]
		Int32 OnFocusChangeIS( [MarshalAs(UnmanagedType.IUnknown)] Object punkObj, Int32 fSetFocus);
	}


    [ComImport, TypeLibType((short)0x1050), Guid("EAB22AC1-30C1-11CF-A7EB-0000C05BAE0B")]
    public interface IWebBrowser {
        [DispId(100)]
        void GoBack();
        [DispId(0x65)]
        void GoForward();
        [DispId(0x66)]
        void GoHome();
        [DispId(0x67)]
        void GoSearch();
        [DispId(0x68)]
        void Navigate([In, MarshalAs(UnmanagedType.BStr)] string URL, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Flags, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object TargetFrameName, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object PostData, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Headers);
        [DispId(-550)]
        void Refresh();
        [DispId(0x69)]
        void Refresh2([In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Level);
        [DispId(0x6a)]
        void Stop();
        [DispId(200)]
        object Application { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(200)] get; }
        [DispId(0xc9)]
        object Parent { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xc9)] get; }
        [DispId(0xca)]
        object Container { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xca)] get; }
        [DispId(0xcb)]
        object Document { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xcb)] get; }
        [DispId(0xcc)]
        bool TopLevelContainer { [DispId(0xcc)] get; }
        [DispId(0xcd)]
        string Type { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0xcd)] get; }
        [DispId(0xce)]
        int Left { [DispId(0xce)] get; [param: In] [DispId(0xce)] set; }
        [DispId(0xcf)]
        int Top { [DispId(0xcf)] get; [param: In] [DispId(0xcf)] set; }
        [DispId(0xd0)]
        int Width { [DispId(0xd0)] get; [param: In] [DispId(0xd0)] set; }
        [DispId(0xd1)]
        int Height { [DispId(0xd1)] get; [param: In] [DispId(0xd1)] set; }
        [DispId(210)]
        string LocationName { [return: MarshalAs(UnmanagedType.BStr)] [DispId(210)] get; }
        [DispId(0xd3)]
        string LocationURL { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0xd3)] get; }
        [DispId(0xd4)]
        bool Busy { [DispId(0xd4)] get; }
    }

    [ComImport, Guid("0002DF05-0000-0000-C000-000000000046"), TypeLibType((short)0x1050)]
    public interface IWebBrowserApp : IWebBrowser {
        [DispId(100)]
        void GoBack();
        [DispId(0x65)]
        void GoForward();
        [DispId(0x66)]
        void GoHome();
        [DispId(0x67)]
        void GoSearch();
        [DispId(0x68)]
        void Navigate([In, MarshalAs(UnmanagedType.BStr)] string URL, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Flags, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object TargetFrameName, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object PostData, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Headers);
        [DispId(-550)]
        void Refresh();
        [DispId(0x69)]
        void Refresh2([In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Level);
        [DispId(0x6a)]
        void Stop();
        [DispId(200)]
        object Application { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(200)] get; }
        [DispId(0xc9)]
        object Parent { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xc9)] get; }
        [DispId(0xca)]
        object Container { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xca)] get; }
        [DispId(0xcb)]
        object Document { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xcb)] get; }
        [DispId(0xcc)]
        bool TopLevelContainer { [DispId(0xcc)] get; }
        [DispId(0xcd)]
        string Type { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0xcd)] get; }
        [DispId(0xce)]
        int Left { [DispId(0xce)] get; [param: In] [DispId(0xce)] set; }
        [DispId(0xcf)]
        int Top { [DispId(0xcf)] get; [param: In] [DispId(0xcf)] set; }
        [DispId(0xd0)]
        int Width { [DispId(0xd0)] get; [param: In] [DispId(0xd0)] set; }
        [DispId(0xd1)]
        int Height { [DispId(0xd1)] get; [param: In] [DispId(0xd1)] set; }
        [DispId(210)]
        string LocationName { [return: MarshalAs(UnmanagedType.BStr)] [DispId(210)] get; }
        [DispId(0xd3)]
        string LocationURL { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0xd3)] get; }
        [DispId(0xd4)]
        bool Busy { [DispId(0xd4)] get; }
        [DispId(300)]
        void Quit();
        [DispId(0x12d)]
        void ClientToWindow([In, Out] ref int pcx, [In, Out] ref int pcy);
        [DispId(0x12e)]
        void PutProperty([In, MarshalAs(UnmanagedType.BStr)] string Property, [In, MarshalAs(UnmanagedType.Struct)] object vtValue);
        [return: MarshalAs(UnmanagedType.Struct)]
        [DispId(0x12f)]
        object GetProperty([In, MarshalAs(UnmanagedType.BStr)] string Property);
        [DispId(0)]
        string Name { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0)] get; }
        [DispId(-515)]
        int HWND { [DispId(-515)] get; }
        [DispId(400)]
        string FullName { [return: MarshalAs(UnmanagedType.BStr)] [DispId(400)] get; }
        [DispId(0x191)]
        string Path { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0x191)] get; }
        [DispId(0x192)]
        bool Visible { [DispId(0x192)] get; [param: In] [DispId(0x192)] set; }
        [DispId(0x193)]
        bool StatusBar { [DispId(0x193)] get; [param: In] [DispId(0x193)] set; }
        [DispId(0x194)]
        string StatusText { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0x194)] get; [param: In, MarshalAs(UnmanagedType.BStr)] [DispId(0x194)] set; }
        [DispId(0x195)]
        int ToolBar { [DispId(0x195)] get; [param: In] [DispId(0x195)] set; }
        [DispId(0x196)]
        bool MenuBar { [DispId(0x196)] get; [param: In] [DispId(0x196)] set; }
        [DispId(0x197)]
        bool FullScreen { [DispId(0x197)] get; [param: In] [DispId(0x197)] set; }
    }

    public enum OLECMDF {
        OLECMDF_SUPPORTED = 1,

        OLECMDF_ENABLED = 2,

        OLECMDF_LATCHED = 4,

        OLECMDF_NINCHED = 8,

        OLECMDF_INVISIBLE = 16,

        OLECMDF_DEFHIDEONCTXTMENU = 32
    }


    public enum OLECMDID {
        OLECMDID_OPEN = 1,
        OLECMDID_NEW = 2,
        OLECMDID_SAVE = 3,
        OLECMDID_SAVEAS = 4,
        OLECMDID_SAVECOPYAS = 5,
        OLECMDID_PRINT = 6,
        OLECMDID_PRINTPREVIEW = 7,
        OLECMDID_PAGESETUP = 8,
        OLECMDID_SPELL = 9,
        OLECMDID_PROPERTIES = 10,
        OLECMDID_CUT = 11,
        OLECMDID_COPY = 12,
        OLECMDID_PASTE = 13,
        OLECMDID_PASTESPECIAL = 14,
        OLECMDID_UNDO = 15,
        OLECMDID_REDO = 16,
        OLECMDID_SELECTALL = 17,
        OLECMDID_CLEARSELECTION = 18,
        OLECMDID_ZOOM = 19,
        OLECMDID_GETZOOMRANGE = 20,
        OLECMDID_UPDATECOMMANDS = 21,
        OLECMDID_REFRESH = 22,
        OLECMDID_STOP = 23,
        OLECMDID_HIDETOOLBARS = 24,
        OLECMDID_SETPROGRESSMAX = 25,
        OLECMDID_SETPROGRESSPOS = 26,
        OLECMDID_SETPROGRESSTEXT = 27,
        OLECMDID_SETTITLE = 28,
        OLECMDID_SETDOWNLOADSTATE = 29,
        OLECMDID_STOPDOWNLOAD = 30,
        OLECMDID_ONTOOLBARACTIVATED = 31,
        OLECMDID_FIND = 32,
        OLECMDID_DELETE = 33,
        OLECMDID_HTTPEQUIV = 34,
        OLECMDID_HTTPEQUIV_DONE = 35,
        OLECMDID_ENABLE_INTERACTION = 36,
        OLECMDID_ONUNLOAD = 37,
        OLECMDID_PROPERTYBAG2 = 38,
        OLECMDID_PREREFRESH = 39,
        OLECMDID_SHOWSCRIPTERROR = 40,
        OLECMDID_SHOWMESSAGE = 41,
        OLECMDID_SHOWFIND = 42,
        OLECMDID_SHOWPAGESETUP = 43,
        OLECMDID_SHOWPRINT = 44,
        OLECMDID_CLOSE = 45,
        OLECMDID_ALLOWUILESSSAVEAS = 46,
        OLECMDID_DONTDOWNLOADCSS = 47,
        OLECMDID_UPDATEPAGESTATUS = 48,
        OLECMDID_PRINT2 = 49,
        OLECMDID_PRINTPREVIEW2 = 50,
        OLECMDID_SETPRINTTEMPLATE = 51,
        OLECMDID_GETPRINTTEMPLATE = 52,
        OLECMDID_PAGEACTIONBLOCKED = 55,
        OLECMDID_PAGEACTIONUIQUERY = 56,
        OLECMDID_FOCUSVIEWCONTROLS = 57,
        OLECMDID_FOCUSVIEWCONTROLSQUERY = 58,
        OLECMDID_SHOWPAGEACTIONMENU = 59,
     //From: http://stackoverflow.com/questions/738232/zoom-in-on-a-web-page-using-webbrowser-net-control
     OLECMDID_OPTICAL_ZOOM = 63,
        OLECMDID_OPTICAL_GETZOOMRANGE = 64,
    }

    public enum OLECMDEXECOPT {
        OLECMDEXECOPT_DODEFAULT = 0,
        OLECMDEXECOPT_PROMPTUSER = 1,
        OLECMDEXECOPT_DONTPROMPTUSER = 2,
        OLECMDEXECOPT_SHOWHELP = 3
    }

    public enum tagREADYSTATE {
        READYSTATE_UNINITIALIZED = 0,
        READYSTATE_LOADING = 1,
        READYSTATE_LOADED = 2,
        READYSTATE_INTERACTIVE = 3,
        READYSTATE_COMPLETE = 4
    }

    [ComImport, Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E"), TypeLibType((short)0x1050)]
    public interface IWebBrowser2 : IWebBrowserApp {
        [DispId(100)]
        void GoBack();
        [DispId(0x65)]
        void GoForward();
        [DispId(0x66)]
        void GoHome();
        [DispId(0x67)]
        void GoSearch();
        [DispId(0x68)]
        void Navigate([In, MarshalAs(UnmanagedType.BStr)] string URL, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Flags, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object TargetFrameName, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object PostData, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Headers);
        [DispId(-550)]
        void Refresh();
        [DispId(0x69)]
        void Refresh2([In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Level);
        [DispId(0x6a)]
        void Stop();
        [DispId(200)]
        object Application { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(200)] get; }
        [DispId(0xc9)]
        object Parent { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xc9)] get; }
        [DispId(0xca)]
        object Container { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xca)] get; }
        [DispId(0xcb)]
        object Document { [return: MarshalAs(UnmanagedType.IDispatch)] [DispId(0xcb)] get; }
        [DispId(0xcc)]
        bool TopLevelContainer { [DispId(0xcc)] get; }
        [DispId(0xcd)]
        string Type { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0xcd)] get; }
        [DispId(0xce)]
        int Left { [DispId(0xce)] get; [param: In] [DispId(0xce)] set; }
        [DispId(0xcf)]
        int Top { [DispId(0xcf)] get; [param: In] [DispId(0xcf)] set; }
        [DispId(0xd0)]
        int Width { [DispId(0xd0)] get; [param: In] [DispId(0xd0)] set; }
        [DispId(0xd1)]
        int Height { [DispId(0xd1)] get; [param: In] [DispId(0xd1)] set; }
        [DispId(210)]
        string LocationName { [return: MarshalAs(UnmanagedType.BStr)] [DispId(210)] get; }
        [DispId(0xd3)]
        string LocationURL { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0xd3)] get; }
        [DispId(0xd4)]
        bool Busy { [DispId(0xd4)] get; }
        [DispId(300)]
        void Quit();
        [DispId(0x12d)]
        void ClientToWindow([In, Out] ref int pcx, [In, Out] ref int pcy);
        [DispId(0x12e)]
        void PutProperty([In, MarshalAs(UnmanagedType.BStr)] string Property, [In, MarshalAs(UnmanagedType.Struct)] object vtValue);
        [return: MarshalAs(UnmanagedType.Struct)]
        [DispId(0x12f)]
        object GetProperty([In, MarshalAs(UnmanagedType.BStr)] string Property);
        [DispId(0)]
        string Name { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0)] get; }
        [DispId(-515)]
        int HWND { [DispId(-515)] get; }
        [DispId(400)]
        string FullName { [return: MarshalAs(UnmanagedType.BStr)] [DispId(400)] get; }
        [DispId(0x191)]
        string Path { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0x191)] get; }
        [DispId(0x192)]
        bool Visible { [DispId(0x192)] get; [param: In] [DispId(0x192)] set; }
        [DispId(0x193)]
        bool StatusBar { [DispId(0x193)] get; [param: In] [DispId(0x193)] set; }
        [DispId(0x194)]
        string StatusText { [return: MarshalAs(UnmanagedType.BStr)] [DispId(0x194)] get; [param: In, MarshalAs(UnmanagedType.BStr)] [DispId(0x194)] set; }
        [DispId(0x195)]
        int ToolBar { [DispId(0x195)] get; [param: In] [DispId(0x195)] set; }
        [DispId(0x196)]
        bool MenuBar { [DispId(0x196)] get; [param: In] [DispId(0x196)] set; }
        [DispId(0x197)]
        bool FullScreen { [DispId(0x197)] get; [param: In] [DispId(0x197)] set; }
        [DispId(500)]
        void Navigate2([In, MarshalAs(UnmanagedType.Struct)] ref object URL, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Flags, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object TargetFrameName, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object PostData, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object Headers);
        [DispId(0x1f5)]
        OLECMDF QueryStatusWB([In] OLECMDID cmdID);
        [DispId(0x1f6)]
        void ExecWB([In] OLECMDID cmdID, [In] OLECMDEXECOPT cmdexecopt, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object pvaIn, [In, Out, Optional, MarshalAs(UnmanagedType.Struct)] ref object pvaOut);
        [DispId(0x1f7)]
        void ShowBrowserBar([In, MarshalAs(UnmanagedType.Struct)] ref object pvaClsid, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object pvarShow, [In, Optional, MarshalAs(UnmanagedType.Struct)] ref object pvarSize);
        [DispId(-525)]
        tagREADYSTATE ReadyState { [DispId(-525), TypeLibFunc((short)4)] get; }
        [DispId(550)]
        bool Offline { [DispId(550)] get; [param: In] [DispId(550)] set; }
        [DispId(0x227)]
        bool Silent { [DispId(0x227)] get; [param: In] [DispId(0x227)] set; }
        [DispId(0x228)]
        bool RegisterAsBrowser { [DispId(0x228)] get; [param: In] [DispId(0x228)] set; }
        [DispId(0x229)]
        bool RegisterAsDropTarget { [DispId(0x229)] get; [param: In] [DispId(0x229)] set; }
        [DispId(0x22a)]
        bool TheaterMode { [DispId(0x22a)] get; [param: In] [DispId(0x22a)] set; }
        [DispId(0x22b)]
        bool AddressBar { [DispId(0x22b)] get; [param: In] [DispId(0x22b)] set; }
        [DispId(0x22c)]
        bool Resizable { [DispId(0x22c)] get; [param: In] [DispId(0x22c)] set; }
    }


    public struct POINT
	{
		public Int32		x;
		public Int32		y;
	}

	public struct MSG 
	{
		public IntPtr		hwnd;
		public UInt32		message;
		public UInt32		wParam;
		public Int32		lParam;
		public UInt32		time;
		public POINT		pt;
	}

}