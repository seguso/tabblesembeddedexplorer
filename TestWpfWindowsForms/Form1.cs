﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using Microsoft.FSharp.Core;
using Microsoft.FSharp.Collections;
using TabblesLibCSharp;
using Microsoft.FSharp.Control;

namespace TestWpfWindowsForms
{
    public partial class Form1 : Form
    {

        
        public Form1()
        {
            InitializeComponent();

        }

        public async Task asyncStuffDebug(DockPanel dp)
        {
            await replicateTabblesInit();

            for (int i = 0; i < 200; i++)
            {

                var tex = new TextBlock { Text = "test item 3 " + i.ToString() };

                dp.Children.Add(tex);
                DockPanel.SetDock(tex, System.Windows.Controls.Dock.Top);


            }

        }



        private static async Task replicateTabblesInit()
        {
            if (null == System.Windows.Application.Current)
            {
                new System.Windows.Application(); // otherwise it crashes when in Tabbles core I call Application.Current which is null
            }

            Tabbles_decl.gContextOfDispatcher = System.Threading.SynchronizationContext.Current;

            Misc.gWndQuickTag = new wnd_add_or_combine_tabbles(); // here it is only instantiated in order for loadImages not to crash

            await Tabbles_decl.startAsTaskSimple(Tabbles_logic.loadImages);


        }

                /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private async void InitializeComponent()
        {
            this.host1 = new System.Windows.Forms.Integration.ElementHost();
            this.btnGo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // host1
            // 
            //this.host1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            //    | System.Windows.Forms.AnchorStyles.Left) 
            //    | System.Windows.Forms.AnchorStyles.Right)));

            host1.Dock = DockStyle.Fill;
            this.host1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.host1.Location = new System.Drawing.Point(12, 12);
            this.host1.Name = "host1";
            //this.host1.Size = new System.Drawing.Size(423, 316);
            this.host1.TabIndex = 0;
            this.host1.Text = "elementHost1";
            this.host1.Child = null;
            // 
            // btnGo
            // 
            this.btnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGo.Location = new System.Drawing.Point(360, 334);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 1;
            this.btnGo.Text = "button1";
            this.btnGo.UseVisualStyleBackColor = true;
            
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 369);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.host1);
            this.Name = "Form1";
            this.Text = "Form1";
            

            /////////////////
            //var host = new ElementHost();

            this.BackColor = System.Drawing.Color.AliceBlue;


            host1.BackColor = System.Drawing.Color.Red;

            this.Controls.Add(host1);


            var sv = new ScrollViewer();
            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            host1.Child = sv;

            var dp = new System.Windows.Controls.DockPanel();
            dp.LastChildFill = true;
            dp.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 220, 255));

            sv.Content = dp;




            //for (int i = 0; i < 200; i++)
            //{

            //    var tex = new TextBlock();

            //    dp.Children.Add(tex);
            //    tex.Text = "item " + i.ToString();
            //    DockPanel.SetDock(tex, System.Windows.Controls.Dock.Top);


            //}



            int barWidth = 220;
            var minHt = 500;


            await asyncStuffDebug(dp);


            //this.MinSize = new System.Drawing.Size(minHt, barWidth);
            //////this.IntegralSize = new System.Drawing.Size(400, 300);
            ////this.Size = new System.Drawing.Size(minHt, barWidth);

            ////this.MinSize = new System.Drawing.Size(150, 24);
            //this.Name = "TabblesBar";
            ////this.Size = new System.Drawing.Size(150, 24);
            //this.Title = "Tabbles";


            this.ResumeLayout(false);
        }

    }
}
