﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleBars
{
    public class Utils
    {

        public static Dictionary<Exception, string> stackTraceOfExc = new Dictionary<Exception, string>();

        public static string getPathRootMaybe(string s)
        {
            if (String.IsNullOrEmpty( s))
                return null;

            try
            {

                return System.IO.Path.GetPathRoot(s);
            }
            catch(Exception ex )
            {
                if (ex is ArgumentException || ex is System.IO.PathTooLongException)
                    return null;

                try
                {
                    stackTraceOfExc[ex] = ex.StackTrace;
                }
                catch { }

                throw;
            }
        }


        public static bool directoryExists(string d)
        {
            return System.IO.Directory.Exists(d);
        }

        public static bool fileExists(string d)
        {
            return System.IO.File.Exists(d);
        }
    }
}
