﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BandObjectLib;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Diagnostics;

namespace SampleBars {
    [Guid("AE07101B-46D4-4a9A-AF68-0331EA26E115"), ComVisible(true), ClassInterface(ClassInterfaceType.None)]
    public class TabblesLoader : IObjectWithSite {
        private IWebBrowser2 explorer;
        private const string BHOKEYNAME = @"Software\Microsoft\Windows\CurrentVersion\Explorer\Browser Helper Objects\";
        private const int E_FAIL = unchecked((int)0x80004005);

        [ComRegisterFunction]
        public static void Register(Type t) {
            logString("TabblesLoader::register");
            string name = t.GUID.ToString("B");
            using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(@"CLSID\" + name)) {
                key.SetValue(null, "TabblesBar AutoLoader");
                key.SetValue("MenuText", "Tabbles AutoLoader");
                key.SetValue("HelpText", "Tabbles AutoLoader");
            }
            Registry.LocalMachine.CreateSubKey(BHOKEYNAME + name);
        }

        [ComUnregisterFunction]
        public static void Unregister(Type t) {
            using (RegistryKey key = Registry.LocalMachine.CreateSubKey(BHOKEYNAME)) {
                key.DeleteSubKey(t.GUID.ToString("B"), false);
            }
        }

        public void SetSite(object site) {
            //logString("TabblesLoader::SetSite");
            explorer = site as IWebBrowser2;
            if (explorer == null || Process.GetCurrentProcess().ProcessName == "iexplore") {
                Marshal.ThrowExceptionForHR(E_FAIL);
            } else {
                ActivateIt();
            }
        }

        public void GetSite(ref Guid guid, out object ppvSite) {
            ppvSite = explorer;
        }

        private static void logString(string str) {

            try {
                System.IO.StreamWriter w = new System.IO.StreamWriter("C:\\logs\\log.txt", true);
                //MessageBox.Show(str);
                w.WriteLine(str);
                w.Flush();
                w.Close();
            } catch (Exception ex) {

            }

        }

        private void ActivateIt() {
            //logString("TabblesLoader::ActivateIt");
            //string installDateString;
            //DateTime installDate;
            //string minDate = DateTime.MinValue.ToString();
            //using (RegistryKey key = Registry.LocalMachine.OpenSubKey(RegConst.Root)) {
            //    installDateString = key == null ? minDate : (string)key.GetValue("InstallDate", minDate);
            //    installDate = DateTime.Parse(installDateString);
            //}
            //using (RegistryKey key = Registry.CurrentUser.CreateSubKey(RegConst.Root)) {
            //    DateTime lastActivation = DateTime.Parse((string)key.GetValue("ActivationDate", minDate));
            //    if (installDate.CompareTo(lastActivation) <= 0) return;

            object pvaTabBar = new Guid("{AE07101B-46D4-4a98-AF68-0333EA26E113}").ToString("B");
            object pvarShow = true;
            object pvarSize = null;
            try {
                //logString("TabblesLoader::ShowBrowserBar");
                explorer.ShowBrowserBar(pvaTabBar, pvarShow, pvarSize);
            } catch (COMException ex) {
                logString("explorer.ShowBrowserBar threw exception: " + ex);
            }
                
            //}
        }
    }

}
